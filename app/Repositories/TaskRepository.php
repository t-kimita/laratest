<?php
namespace App\Repositories;

use App\User;

class TaskRepository extends ReposBase
{
    /**
     * 指定ユーザーの全タスク取得
     *
     * @param  User  $user
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function forUser(User $user)
    {
        return $user->tasks()
                    ->orderBy('created_at', 'asc')
                    ->get();
    }
}