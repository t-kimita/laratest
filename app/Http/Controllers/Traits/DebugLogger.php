<?php

namespace App\Http\Controllers\Traits;

use Monolog\Logger as Logger;
use Monolog\Handler\StreamHandler;

trait DebugLogger
{
    /**
     * Monologのカスタムログ（debug出力用）
     *
     * @var Logger
     */
    protected $custom_log;

    public function initCustomLog()
    {
        $this->custom_log = new Logger('forDebug');
        $this->custom_log->pushHandler(new StreamHandler(getcwd()."/../storage/logs/debug.log", Logger::DEBUG));
    }
}
