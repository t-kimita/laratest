<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HogeController extends Controller
{
    public function index()
    {
      $this->custom_log->addDebug('debug_bar', ['getcwd'=>getcwd()]);
      return view('hoge.index', ['cwd'=>getcwd()]);
    }

    public function test()
    {
      return view('hoge.test');
    }
}
