<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Task;
use App\Repositories\TaskRepository;

class TaskController extends Controller
{
    /**
     * タスクリポジトリーインスタンス
     *
     * @var TaskRepository
     */
    protected $tasks;

    /**
     * 新しいコントローラインスタンスの生成
     *
     * @param TaskRepository $tasks
     * @return void
     */
    public function __construct(TaskRepository $tasks)
    {
        parent::__construct();
        $this->middleware('auth');

        $this->tasks = $tasks;
    }

    /**
     * ユーザーの全タスクをリスト表示
     *
     * @param  Request  $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->custom_log->addDebug(__METHOD__);

        return view('tasks.index', [
            'tasks' => $this->tasks->forUser($request->user()),
        ]);
    }

    /**
     * 新タスク作成
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->custom_log->addDebug(__METHOD__);
        $this->validate($request, [
            'name' => 'required|max:191',
        ]);

        $request->user()->tasks()->create([
            'name' => $request->name,
        ]);

        return redirect('/tasks');
    }

    /**
     * 指定タスクの削除
     *
     * @param  Request  $request
     * @param  Task  $task
     * @return Response
     */
    public function destroy(Request $request, Task $task)
    {
        /*
         * for Debug: 下記２行は、出力で扱う内容は同じだが、出力先や表現方法が異なる
         */
//         // Monolog\Logger 出力内容が一行にまとめられる
//         $this->custom_log->addDebug(__METHOD__,['isOwner'=>print_r($this->authorize('isOwner', $task), true)]);
        // Laravelのヘルパ関数
//         logger()->debug(__METHOD__,['isOwner'=>print_r($this->authorize('isOwner', $task), true)]);
        $this->authorize('isOwner', $task);

        $task->delete();

        return redirect('/tasks');
    }

}
