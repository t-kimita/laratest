<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Http\Controllers\Traits\DebugLogger;
use App\Repositories\ReposBase;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, DebugLogger;

    /**
     * @param ReposBase $repo
     */
    public function __construct(ReposBase $repo = null)
    {
        $this->initCustomLog();
    }
}
